import { Component, OnInit } from '@angular/core';
import * as data from '../data/sample_data.json';
import { PagerService } from '../services/index';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'person-app';
  diplayColumns: string[] = ['name', 'phone', 'email', 'company', 'date_entry', 'org_num', 'address_1', 'city', 'zip', 'geo', 'pan', 'pin', 'id', 'status', 'fee', 'guid', 'date_exit', 'date_first', 'date_recent', 'url'];

  // array of all items to be paged
  persons: any[];

  // pager object
  pager: any = {};

  // paged items
  pagedItems: any[];

  constructor(private pagerService: PagerService) {
  }

  ngOnInit(): void {
    // console.log(data);
    this.persons = (data as any).default;
    // console.log(Object.keys(this.persons[0]));
    this.setPage(1);
  }

  setPage(page: number, pageSize: number = 25): void {
    // get pager object from service
    this.pager = this.pagerService.getPager(this.persons.length, page, pageSize);

    // get current page of items
    this.pagedItems = this.persons.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  onSubmit(id: number): void {
    console.log(id);
  }

  onPageSelection(event: any): void{
    const value = event.target.value;
    this.setPage(1, value);
    console.log(value);
  }
}
